package fr.umfds.TP_NOTE_LS;

public class Enseignant extends Personne {
	
	private Groupe groupe;
	private Sujet sujet;
	
	public Enseignant (String n, String p, int i,Groupe g, Sujet s) {
		super(n, p, i);
		groupe = g;
		sujet = s;
	}

}
