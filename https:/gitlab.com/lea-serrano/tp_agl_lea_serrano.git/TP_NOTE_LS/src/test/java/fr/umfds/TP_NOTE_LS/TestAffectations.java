package fr.umfds.TP_NOTE_LS;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

class TestAffectations {
	
	static Groupe grp1;
	static Sujet suj1;
	static Enseignant e1;
	
	@BeforeAll
	static void initAll() {
		e1 = new Enseignant("S", "L", 1, grp1, suj1);
		grp1 = new Groupe(1, "LesMeilleurs", e1, suj1);
		suj1 = new Sujet (1, "Faire du code", grp1);
	}
	
	@Test
	public void testAffectationGroupe() {
		suj1.affecterGroupe(grp1);
		grp1.affecterSujet(suj1);
		assertEquals(grp1, suj1.getGroupe());
	}
	
	@Test
	public void testAffectationSujet() {
		suj1.affecterGroupe(grp1);
		grp1.affecterSujet(suj1);
		assertEquals(suj1, grp1.getSujet());
	}
	
}